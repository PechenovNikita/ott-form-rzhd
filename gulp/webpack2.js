var webpack = require('webpack');

module.exports = {

  output    : {
    filename          : 'rhzd-search.js',
    sourceMapFilename : 'rhzd-search.map'
  },
  watch     : true,
  module    : {
    loaders : [
      {
        test    : /.jsx?$/,
        loader  : 'babel-loader',
        exclude : /node_modules/,

        query : {
          compact : false,
          presets : ['es2015', 'react']
        }
      }
    ]
  },
  // devtool   : 'source-map',
  externals : {
    'react-dom' : 'ReactDOM',
    'react'     : 'React',
    'pikaday'   : 'Pikaday',
    'loader'    : 'Loader'
  },
  plugins   : [
    new webpack.optimize.UglifyJsPlugin({
      compress : {
        warnings : true
      }
    })
  ],
  resolve   : {
    extensions : ['', '.js', '.jsx']
  }
};