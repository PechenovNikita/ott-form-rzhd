// var path = '/form/dest/';
// var path = '/ott/rzhd-form-2016/';
var path = '/';

module.exports = {
  path : {
    main : path,
    css  : path + 'css/',
    js   : path + 'js/',
    images  : path + 'images/',
    imagesForLess : '/dest/images/'
  }
};