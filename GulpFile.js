var gulp = require('gulp')
  , less = require('gulp-less')
  , replace = require('gulp-replace')
  , base64 = require('gulp-base64')
  , lessData = require('gulp-less-data')
  , uglify = require('gulp-uglifyjs')
  , rename = require('gulp-rename')
  , watch = require('gulp-watch')
  , concat = require('gulp-concat')
  , cleanCSS = require('gulp-clean-css')
  , pug = require('gulp-pug')
  , webpack = require('gulp-webpack');

var insert = require('gulp-insert');

var config = require('./gulp/_config');
var lessD = {
  imagesPath         : '"' + config.path.imagesForLess + '"',
  imagesPathNoBase64 : '"' + config.path.images + '"',
};

gulp.task('js:station', function () {
  return gulp.src('./src/js/assets/station.js')
    .pipe(insert.wrap('let Stations = ', ' export default Stations;'))
    .pipe(rename('station-compile.js'))
    .pipe(gulp.dest('./src/js/assets'));
});

gulp.task('js:pikaday', function () {
  return gulp.src(['./src/js/assets/pikaday.js'])
    .pipe(gulp.dest('./dest/js/assets'))
    .pipe(uglify())
    .pipe(rename('pikaday.min.js'))
    .pipe(gulp.dest('./dest/js/assets'));
});

gulp.task('js:pikaday', function () {
  return gulp.src(['./src/js/assets/pikaday.js'])
    .pipe(gulp.dest('./dest/js/assets'))
    .pipe(uglify())
    .pipe(rename('pikaday.min.js'))
    .pipe(gulp.dest('./dest/js/assets'));
});

gulp.task('js:concat:libs', function () {
  return gulp.src(['./dest/js/assets/pikaday.min.js', './dest/js/assets/react.min.js', './dest/js/assets/react-dom.min.js'])
    .pipe(concat('libs.js'))
    .pipe(gulp.dest('./dest/js/assets'));
});

gulp.task('js:compile', function () {
  return gulp.src(['./src/js/rhzd-search_2.js'])
    .pipe(webpack(require('./gulp/webpack2')))
    .pipe(replace('#jsPath#', config.path.js))
    .pipe(replace('#mainPath#', config.path.main))
    // .pipe(rename('rhzd-search_2.js'))
    .pipe(gulp.dest('./dest/js'));
});

gulp.task('js', gulp.series(gulp.parallel('js:station', gulp.series('js:pikaday', 'js:concat:libs')), 'js:compile'));

gulp.task('less', function () {
  return gulp.src('./src/less/index.less')
    .pipe(lessData(lessD))
    .pipe(less())
    .pipe(base64({
      baseDir : '/Users/zachot/Documents/Projects/ott/form/',
      debug   : true
    }))
    .pipe(rename('rhzd-search.css'))
    .pipe(gulp.dest('./dest/css'))
    .pipe(cleanCSS())
    .pipe(rename('rhzd-search.min.css'))
    .pipe(gulp.dest('./dest/css'));
});

gulp.task('pug', function () {
  return gulp.src('./src/pug/index.pug')
    .pipe(pug({
      pretty : true,
      locals : {config : require('./gulp/_config')}
    }))
    .pipe(gulp.dest('./dest'));
});

gulp.task('watch', gulp.parallel(function () {
  return watch('./src/less/**/*.less', {verbose : true}, function () {
    return gulp.src('./src/less/index.less')
      .pipe(lessData(lessD))
      .pipe(less())
      .pipe(base64({
        baseDir : '/Users/zachot/Documents/Projects/ott/form/',
        debug   : true
      }))
      .pipe(rename('rhzd-search.css'))
      .pipe(gulp.dest('./dest/css'))
      .pipe(cleanCSS())
      .pipe(rename('rhzd-search.min.css'))
      .pipe(gulp.dest('./dest/css'));
  })
}, function () {
  return watch(['./src/js/**/*.js', './src/jsx2/**/*.jsx'], {verbose : true}, function () {
    return gulp.src('./src/js/rhzd-search_2.js')
      .pipe(webpack(require('./gulp/webpack2')))
      .pipe(replace('#jsPath#', config.path.js))
      .pipe(replace('#mainPath#', config.path.main))
      // .pipe(rename('rhzd-search_2.js'))
      .pipe(gulp.dest('./dest/js'));
  })
}));