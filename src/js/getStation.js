"use strict";
import JSONP from './JSONP';
import _config from './_config';

import translate from './assets/translateBoth'
/**
 *
 * @type {Object.<number, MyOttStation>}
 */
let tempIdToStation = {};
let tempCodeToStation = {};

/**
 *
 * @type {Object.<string, OttStation>}
 */
window.Stations = undefined;

JSONP.ajax(_config.urlStationsJs, function (response) {
  eval('window.Stations = ' + response);
  STData.load();
});

let functions = [];

var STData = {};

/**
 *
 */
STData.load = function () {
  functions.forEach(function (callback) {
    callback();
  });
};

/**
 *
 * @returns {boolean}
 */
STData.loaded = function () {
  return !!Stations;
};

/**
 *
 * @param {?string} name
 * @param {?string|?number} code
 * @param {?string} ident
 * @param {?function} callback
 * @param {boolean=} doCallback
 * @returns {*}
 */
STData.getStationByAll = function (name, code, ident, callback, doCallback) {
  if (Stations) {
    let result;

    if (ident)
      result = STData.getStationByName(ident);

    if (!result && name) {
      ident = translate(name);
      result = STData.getStationByName(ident);
    }

    if (!result && code) {
      result = STData.getStationByCode(code);
    }

    if (result && callback && doCallback)
      callback(result);
    return result;

  } else {
    functions.push(STData.getStationByAll.bind(STData, name, code, ident, callback, true))
  }
}
;

STData.getStationByName = function (name, callback, doCallback) {
  if (Stations) {
    var result = null;

    if (Stations.hasOwnProperty(name)) {
      Stations[name].url = name;
      result = Stations[name];
    }

    if (result && callback && doCallback)
      callback(result);
    return result;

  } else {
    functions.push(STData.getStationByName.bind(STData, name, callback, true))
  }
};

/**
 *
 * @param {number} id
 * @returns {?MyOttStation}
 */
STData.getStation = function (id) {
  if (Stations) {

    if (tempIdToStation[id])
      return tempIdToStation[id];

    for (var name in Stations) {
      if (Stations.hasOwnProperty(name)) {
        if (!tempIdToStation[Stations[name].id]) {
          tempIdToStation[Stations[name].id] = Stations[name];
          tempIdToStation[Stations[name].id].url = name;
        }
        if (Stations[name].id == id) {
          return tempIdToStation[id];
        }
      }
    }

  }

  return null;
};

STData.getStationByCode = function (code, callback, doCallback) {
  if (Stations) {
    var result = null;

    if (tempCodeToStation[code]) {
      result = tempCodeToStation[code];
    } else {
      for (var name in Stations) {
        if (Stations.hasOwnProperty(name)) {
          if (!tempCodeToStation[Stations[name].code]) {
            tempCodeToStation[Stations[name].code] = Stations[name];
            tempCodeToStation[Stations[name].code].url = name;
          }

          if (Stations[name].code == code) {
            result = tempCodeToStation[code];
            break;
          }
        }
      }
    }

    if (result && callback && doCallback)
      callback(result);

    return result;

  } else {
    functions.push(STData.getStationByCode.bind(STData, code, callback, true))
  }

  return null;
};

export default STData;