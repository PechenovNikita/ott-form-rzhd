/**
 * @typedef {Object} AJAXOttCity
 * @property {string} country
 * @property {int} countryId
 * @property {int} id
 * @property {string} name
 */

/**
 * @typedef {Object} AJAXOttResults
 * @property {Array<AJAXOttCity>} result
 * @property {boolean} success
 */

/**
 * @typedef {Object} OttLangString
 * @property {string} en
 * @property {string} ru
 */

/**
 * @typedef {Object} OttStation
 * @property {boolean} active
 * @property {number} code
 * @property {OttLangString} country
 * @property {int} countryId
 * @property {int} id
 * @property {boolean} meta
 * @property {OttLangString} name
 * @property {number} popularity
 * @property {Array<string>} stations
 */

/**
 *
 * @typedef {OttStation} MyOttStation
 * @property {string} url
 */

/**
 * @typedef {Object} StateDataStation
 * @property {?string} name
 * @property {?string} ident
 * @property {?number} id
 * @property {?number} code
 * @property {?MyOttStation} data
 */

require('./classList');

require('./URLParser');
// require('./getStation');
require('./../jsx2/app.jsx');