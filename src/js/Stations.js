"use strict";
import JSONP from './JSONP';
import _config from './_config';
import translateBoth from './mixin/translateBoth'

import Stations from './assets/station-compile';
/**
 *
 * @type {Object.<number, MyOttStation>}
 */
let tempIdToStation = {};
let tempCodeToStation = {};

if (!window.Stations && typeof Stations === "undefined") {
  /**
   *
   * @type {Object.<string, OttStation>}
   */
  window.Stations = undefined;

  JSONP.ajax(_config.urlStationsJs, function (response) {
    eval('window.Stations = ' + response);
    STData.load();
  });

} else if(Stations){
  window.Stations = Stations;
}

let functions = [];

var STData = {};

/**
 *
 */
STData.load = function () {
  functions.forEach(function (callback) {
    callback();
  });
};

/**
 *
 * @returns {boolean}
 */
STData.loaded = function () {
  return !!Stations;
};

/**
 *
 * @param {?string} name
 * @param {?string|?number} code
 * @param {?string} ident
 * @param {?function} callback
 * @param {boolean=} doCallback
 * @returns {*}
 */
STData.getStation = function (name, code, ident, callback, doCallback) {
  if (Stations) {
    let result;

    if (ident)
      result = STData.getStationByIdent(ident);

    if (!result && name) {
      ident = translateBoth(name);
      result = STData.getStationByIdent(ident);
    }

    if (!result && code) {
      result = STData.getStationByCode(code);
    }

    if (result && callback && doCallback)
      callback(result);
    return result;

  } else {
    functions.push(STData.getStation.bind(STData, name, code, ident, callback, true))
  }
};

STData.getStationByIdent = function (name, callback, doCallback) {
  if (Stations) {
    var result = null;

    if (Stations.hasOwnProperty(name)) {
      Stations[name].url = name;
      result = Stations[name];
    }

    if (result && callback && doCallback)
      callback(result);
    return result;

  } else {
    functions.push(STData.getStationByIdent.bind(STData, name, callback, true));
  }
};

/**
 *
 * @param {number} id
 * @returns {?MyOttStation}
 */
STData.getStationById = function (id, callback, doCallback) {
  if (Stations) {
    let result;
    if (tempIdToStation[id]) {

      result = tempIdToStation[id];

    } else {
      for (var name in Stations) {
        if (Stations.hasOwnProperty(name)) {
          if (!tempIdToStation[Stations[name].id]) {

            tempIdToStation[Stations[name].id] = Stations[name];
            tempIdToStation[Stations[name].id].url = name;

          }
          if (Stations[name].id == id) {
            result = tempIdToStation[id];
          }
        }
      }
    }

    if (result && callback && doCallback)
      callback(result);

    return result;

  } else {
    functions.push(STData.getStationById.bind(STData, id, callback, true))
  }

  return null;
};

STData.getStationByCode = function (code, callback, doCallback) {
  if (Stations) {
    var result = null;

    if (tempCodeToStation[code]) {
      result = tempCodeToStation[code];
    } else {
      for (var name in Stations) {
        if (Stations.hasOwnProperty(name)) {
          if (!tempCodeToStation[Stations[name].code]) {
            tempCodeToStation[Stations[name].code] = Stations[name];
            tempCodeToStation[Stations[name].code].url = name;
          }

          if (Stations[name].code == code) {
            result = tempCodeToStation[code];
            break;
          }
        }
      }
    }

    if (result && callback && doCallback)
      callback(result);

    return result;

  } else {
    functions.push(STData.getStationByCode.bind(STData, code, callback, true))
  }

  return null;
};

export default STData;