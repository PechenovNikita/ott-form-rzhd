/**
 * @typedef {Object} AJAXOttCity
 * @property {string} country
 * @property {int} countryId
 * @property {int} id
 * @property {string} name
 */

/**
 * @typedef {Object} AJAXOttResults
 * @property {Array<AJAXOttCity>} result
 * @property {boolean} success
 */

/**
 * @typedef {Object} OttLangString
 * @property {string} en
 * @property {string} ru
 */

/**
 * @typedef {Object} OttStation
 * @property {boolean} active
 * @property {number} code
 * @property {OttLangString} country
 * @property {int} countryId
 * @property {int} id
 * @property {boolean} meta
 * @property {OttLangString} name
 * @property {number} popularity
 * @property {Array<string>} stations
 */

/**
 *
 * @typedef {OttStation} MyOttStation
 * @property {string} url
 */


// Добавляем картинку в кеш браузера - не хочу делать спрайты а svg спрайты не отображаются в IE9 (неверные пути)
// ['cal-drop-yellow.svg', 'cal-drop.svg', 'edit-yellow.svg'].forEach(function (file) {
//   let image = new Image();
//   image.src = '#mainPath#images/' + file;
// });
require('./classList');
require('./getStation');
require('./../jsx/app.jsx');