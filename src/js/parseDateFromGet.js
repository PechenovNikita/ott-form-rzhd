"use strict";

export default function (dateString) {

  if (dateString && dateString.length === 8) {
    var day = parseInt(dateString.slice(0, 2), 10);
    var month = parseInt(dateString.slice(2, 4), 10);
    var year = parseInt(dateString.slice(4, 8), 10);
    return new Date(year, month - 1, day);
  }

  return null;
}