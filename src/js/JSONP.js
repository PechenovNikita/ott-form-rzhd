// https://www.onetwotrip.com/_api/rzd/suggestStations/?searchText=казан&lang=ru&limit=10&flat=true

/**
 *
 * @param {string} url
 * @param {function(Object, number)} done
 * @returns {number}
 * @constructor
 */
var JSONP = function (url, done) {
  var id = Date.now();
  var functionName = "func_" + Date.now();

  window[functionName] = function (result) {
    if (done)
      done(result, id);
  };

  var script = document.createElement('script');
  script.src = url + '&callback=' + functionName;

  document.head.appendChild(script);

  return id;
};

/**
 *
 * @param {string} url
 * @param {function(string, number)} done
 * @returns {number}
 */
JSONP.ajax = function(url, done){
  var id = Date.now();
  var xmlhttp = new XMLHttpRequest();

  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
      if (xmlhttp.status == 200) {
        done(xmlhttp.responseText, id);
      }
      else if (xmlhttp.status == 400) {
        console.log('There was an error 400');
      }
      else {
        console.log('something else other than 200 was returned');
      }
    }
  };

  xmlhttp.open("GET", url, true);
  xmlhttp.send();

  return id;
};

/**
 *
 * @param callback
 * @param {string} searchText
 * @param {string=} lang
 * @param {number=} limit
 * @param {boolean=} flat
 * @returns {number}
 */
JSONP.station = function (callback, searchText, lang = 'ru', limit = 10, flat = true) {
  return JSONP('https://www.onetwotrip.com/_api/rzd/suggestStations/?searchText=' + encodeURIComponent(searchText) + '&lang=' + lang + '&limit=' + limit + '&flat=' + flat, callback)
};

/**
 *
 * @param url
 * @param callback
 */
JSONP.getScript = function (url, callback) {
  // console.log('getScript', url);

  var script = document.createElement('script');
  script.src = url;
  script.onload = callback;

  document.head.appendChild(script);
};

export default JSONP;