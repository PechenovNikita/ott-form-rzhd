"use strict";
let parameters = {}
  , update = true;

let url = location.toString();

let callbacksWhenChangeUrl = [];

let currentState = undefined;

var URLParser = {

  setState(state){
    currentState = state;
  },

  getState(){
    return currentState;
  },

  whenChange(callback){
    callbacksWhenChangeUrl.push(callback);
  },
  /**
   *
   * @returns {Object.<string, any>}
   */
  parseGet() {

    let p = {};

    location.search.substr(1).split("&")
      .forEach(function (item) {
        let tmp = item.split("=");

        if (!tmp)
          return;

        let name = tmp[0]
          , value = tmp[1];

        if (value && value.indexOf(',') > 0) {
          value = value.split(',').map(function (str) {
            return decodeURIComponent(str);
          });
        } else {
          value = decodeURIComponent(value)
        }

        if (name && name.indexOf('[') > 0) {
          name = name.split('[');
          name = name[0];

          if (p[name]) {
            if (Array.isArray(p[name])) {
              value = p[name].concat(value);
            } else {
              value = [p[name], value];
            }
          }
        }

        p[name] = value;
      });

    return p;

  },

  /**
   *
   * @returns {{from: string, to: string}}
   */
  parseCityPath() {
    let urls = window.location.pathname.split('/')
      , from = '', to = '';

    if (urls && urls.length > 0) {

      let check = urls[urls.length - 1];
      check = (!check && urls.length > 2 ? urls[urls.length - 2] : check);

      if (check) {
        let cities = check.split('_');

        if (cities && cities.length == 2) {
          from = cities[0];
          to = cities[1];

          from = (from ? from : '');
          to = (to ? to : '');
        }
      }
    }
    return {
      from : from,
      to   : to
    };
  },

  /**
   *
   * @param {string=} parameterName
   * @param {?string=} defaultValue
   * @returns {null|string|number|Object}
   */
  getParameter (parameterName, defaultValue = null) {

    if (update) {
      parameters = URLParser.parseGet();
      update = false;
    }

    if (typeof parameterName === "undefined")
      return parameters;
    if (parameters[parameterName])
      return parameters[parameterName];
    return defaultValue;

  },

  pushState(state){
    checkUrl({state : state});
  }

};

function checkUrl(event) {

  if (event && event.state) {
    URLParser.setState(event.state);
  } else {
    URLParser.setState(null);
  }

  let newUrl = location.toString();
  if (url != newUrl) {
    url = newUrl;

    update = true;

    callbacksWhenChangeUrl.forEach(function (callback) {
      callback();
    });
  }

}

window.addEventListener('popstate', checkUrl);
window.addEventListener('wlRouterDidChange', checkUrl);

// setInterval(checkUrl, 500);

export default URLParser;

// setTimeout(function () {
//   history.pushState({}, '', '#mainPath#train?fromName=%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0&toName=%D0%A1%D0%B0%D0%BD%D0%BA%D1%82-%D0%9F%D0%B5%D1%82%D0%B5%D1%80%D0%B1%D1%83%D1%80%D0%B3&train=020%D0%A3&from=2000001&to=2004000&classes%5B0%5D=4&classes%5B1%5D=6&minCost=2184.1&metaTo=22871&metaFrom=22823&date=08112016&currency=RUB')
// }, 1000);