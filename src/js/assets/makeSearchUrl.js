const translateBoth = require('utility/translateBoth');

const checkZero = (num) => {
  return num < 10 ? `0${num}` : num
};

const stringifyDateReverse = (date) => {
  const day = checkZero(date.getDate());
  const month = checkZero(date.getMonth() + 1);
  const year = date.getFullYear();
  
  return `${day}${month}${year}`;
};

module.exports = ({from, to, fromCode, toCode, date}) => {
  return `/${translateBoth(from)}-${translateBoth(to)}_${fromCode}-${toCode}/#${stringifyDateReverse(date)}`
}


