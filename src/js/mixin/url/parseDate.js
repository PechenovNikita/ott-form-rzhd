"use strict";

const currentYear = (new Date()).getFullYear()
  , monthOnScreen = ['янв', 'февр', 'марта', 'апр', 'мая', 'июня', 'июля', 'авг', 'сен', 'окт', 'нояб', 'дек']
  , monthInField = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

/**
 *
 * @param {?string} dateString
 * @returns {?Date}
 */
export function parse(dateString) {
  if (dateString && dateString.length === 8) {
    var day = parseInt(dateString.slice(0, 2), 10);
    var month = parseInt(dateString.slice(2, 4), 10);
    var year = parseInt(dateString.slice(4, 8), 10);
    return new Date(year, month - 1, day);
  }
  return null;
}

/**
 *
 * @param {?Date} date
 * @returns {?string}
 */
export function stringify(date) {
  if (!date || !(date instanceof Date))
    return null;

  let day = date.getDate()
    , month = date.getMonth() + 1
    , year = date.getFullYear();

  day = day < 10 ? "0" + day : day;
  month = month < 10 ? "0" + month : month;

  return day + '' + month + '' + year;
}

/**
 *
 * @param {?Date} date
 * @param {string[]} months
 * @returns {?string}
 */
function stringifyMode(date, months){
  if (!date || !(date instanceof Date))
    return '';

  let day = date.getDate()
    , month = date.getMonth()
    , year = date.getFullYear();

  let tmp = day + "\u00a0" + months[month];
  if (year != currentYear)
    tmp += ', ' + year + '\u00a0г.';

  return tmp;
}

/**
 *
 * @param {Date} date
 * @returns {?string}
 */
export function stringifyScreen(date) {
  return stringifyMode(date, monthOnScreen);
}

/**
 *
 * @param {Date} date
 * @returns {?string}
 */
export function stringifyDateSelect(date) {
  return stringifyMode(date, monthInField);
}


export default {
  parse               : parse,
  stringify           : stringify,
  stringifyScreen     : stringifyScreen,
  stringifyDateSelect : stringifyDateSelect
}