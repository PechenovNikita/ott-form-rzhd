"use strict";
let parameters;

function parseOneItem(item) {

  let tmp = item.split("=");
  if (!tmp) return;

  let name = tmp[0], value = tmp[1];

  if (value && value.indexOf(',') > 0) {
    value = value.split(',').map(function (str) { return decodeURIComponent(str); });
  } else {
    value = decodeURIComponent(value)
  }

  if (name && decodeURIComponent(name).indexOf('[') > 0) {
    name = decodeURIComponent(name).split('[');
    name = name[0];

    if (parameters[name]) {
      if (Array.isArray(parameters[name])) {
        value = parameters[name].concat(value);
      } else {
        value = [parameters[name], value];
      }
    }
  }

  parameters[name] = value;
}

/**
 *
 * @returns {Object}
 */
export function parse() {
  parameters = {};

  location.search
    .substr(1).split("&")
    .forEach(parseOneItem);

  return parameters;
}
/**
 *
 * @param {string} name
 * @returns {any}
 */
export function get(name) {
  if (parameters && parameters.hasOwnProperty(name))
    return parameters[name];
}

export default {
  get   : get,
  parse : parse
}