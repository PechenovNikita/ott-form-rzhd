"use strict";

export function get() {
  let urls = window.location.pathname.split('/')
    , from = '', to = '';

  if (urls && urls.length > 0) {

    let i = urls.length - 1;

    while (i >= 0) {
      let path = urls[i];
      if (path.indexOf('_') > 0) {
        let cities = path.split('_');
        if (cities && cities.length == 2) {
          from = (cities[0] ? cities[0] : '');
          to = (cities[1] ? cities[1] : '');
          break;
        }
      }
      i--;
    }

  }

  return {
    from : from,
    to   : to
  };
}

export default {
  get : get
}