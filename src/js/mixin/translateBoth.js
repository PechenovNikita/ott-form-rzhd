var ru = {};
var eng = {};

ru["Ё"] = "YO";
ru["Й"] = "I";
ru["Ц"] = "TS";
ru["У"] = "U";
ru["К"] = "K";
ru["Е"] = "E";
ru["Н"] = "N";
ru["Г"] = "G";
ru["Ш"] = "SH";
ru["Щ"] = "SCH";
ru["З"] = "Z";
ru["Х"] = "H";
ru["Ъ"] = "'";
ru["ё"] = "yo";
ru["й"] = "yy";
ru["ц"] = "ys";
ru["у"] = "u";
ru["к"] = "k";
ru["е"] = "e";
ru["н"] = "n";
ru["г"] = "g";
ru["ш"] = "sh";
ru["щ"] = "sch";
ru["з"] = "z";
ru["х"] = "h";
ru["ъ"] = "'";
ru["Ф"] = "F";
ru["Ы"] = "I";
ru["В"] = "V";
ru["А"] = "A";
ru["П"] = "P";
ru["Р"] = "R";
ru["О"] = "O";
ru["Л"] = "L";
ru["Д"] = "D";
ru["Ж"] = "ZH";
ru["Э"] = "E";
ru["ф"] = "f";
ru["ы"] = "yi";
ru["в"] = "v";
ru["а"] = "a";
ru["п"] = "p";
ru["р"] = "r";
ru["о"] = "o";
ru["л"] = "l";
ru["д"] = "d";
ru["ж"] = "zh";
ru["э"] = "e";
ru["Я"] = "Ya";
ru["Ч"] = "CH";
ru["С"] = "S";
ru["М"] = "M";
ru["И"] = "I";
ru["Т"] = "T";
ru["Ь"] = "'";
ru["Б"] = "B";
ru["Ю"] = "YU";
ru["я"] = "ya";
ru["ч"] = "ch";
ru["с"] = "s";
ru["м"] = "m";
ru["и"] = "i";
ru["т"] = "t";
ru["ь"] = "'";
ru["б"] = "b";
ru["ю"] = "yu";
ru["-"] = "-";
ru[" "] = "+";

eng["YO"] = "Ё";
eng["I"] = "Й";
eng["TS"] = "Ц";
eng["U"] = "У";
eng["K"] = "К";
eng["E"] = "Е";
eng["N"] = "Н";
eng["Г"] = "G";
eng["SH"] = "Ш";
eng["SCH"] = "Щ";
eng["Z"] = "З";
eng["H"] = "Х";
eng["'"] = "Ъ";
eng["yo"] = "ё";
eng["ts"] = "ц";
eng["u"] = "у";
eng["k"] = "к";
eng["e"] = "е";
eng["n"] = "н";
eng["g"] = "г";
eng["sh"] = "ш";
eng["sch"] = "щ";
eng["z"] = "з";
eng["h"] = "х";
eng["'"] = "ъ";
eng["F"] = "Ф";
eng["I"] = "Ы";
eng["V"] = "В";
eng["a"] = "А";
eng["P"] = "П";
eng["R"] = "Р";
eng["O"] = "О";
eng["L"] = "Л";
eng["D"] = "Д";
eng["ZH"] = "Ж";
eng["E"] = "Э";
eng["f"] = "ф";
eng["v"] = "в";
eng["a"] = "а";
eng["p"] = "п";
eng["r"] = "р";
eng["o"] = "о";
eng["l"] = "л";
eng["d"] = "д";
eng["zh"] = "ж";
eng["e"] = "е";
eng["Ya"] = "Я";
eng["CH"] = "Ч";
eng["S"] = "С";
eng["M"] = "М";
eng["I"] = "И";
eng["T"] = "Т";
eng["'"] = "Ь";
eng["B"] = "Б";
eng["YU"] = "Ю";
eng["ya"] = "я";
eng["ch"] = "ч";
eng["s"] = "с";
eng["m"] = "м";
eng["i"] = "и";
eng["t"] = "т";
eng["'"] = "ь";
eng["b"] = "б";
eng["yi"] = "ы";
eng["yu"] = "ю";
eng["yy"] = "й";
eng["ys"] = "ц";
eng["-"] = "-";
eng["+"] = " ";

module.exports = function transliterate(word, isEng) {

  var str = word.toLowerCase();

  let a = isEng ? eng : ru
    , group = '';

  let nextWord = str.split('').map(function (letter) {
    if (a[group + letter]) {
      let symbol = a[group + letter];
      group = '';
      return symbol;
    } else {
      group += letter
    }
  }).join('');

  if (isEng) {
    nextWord = nextWord.replace(/зх/g, 'ж')
      .replace(/цх/g, "ч")
      .replace(/шч/g, "щ")
      .replace(/йу/g, "ю")
      .replace(/йа/g, "я")
      .replace(/сх/g, "ш");
  }

  return nextWord;
};
