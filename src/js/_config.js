"use strict";

export default {
  urlRedirect   : '#mainPath#',
  urlStationsJs : '#jsPath#libs/station.js'
}