"use strict";

export default function(component, obj){
  var state = component.state;
  for(var name in obj){
    if(obj.hasOwnProperty(name)){
      state[name] = obj[name];
    }
  }
  component.setState(state);
}