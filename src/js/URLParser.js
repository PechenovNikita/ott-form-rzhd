"use strict";
import parseDate from './mixin/url/parseDate';
import parseParameters from './mixin/url/parseParameters';
import {get as parGet} from './mixin/url/parseParameters';
import parseCityPath from './mixin/url/parseCityPath';

import ST from './Stations';
import _config from './_config';

import translateBoth from './mixin/translateBoth';

let path
  , lastState;

let formData = {
  dateFromUrl : null,
  date        : null,
  from        : null,
  to          : null
};

let pathAdd = _config.urlRedirect;
let functions = [];

function capitalize(string) {
  let temp = string.split('-');
  return temp.map(function (str) {
    return str.charAt(0).toUpperCase() + str.slice(1)
  }).join('-');
}

var LoadedStations = {
  from(data){
    formData.from.data = data;
    update();
  },
  to(data){
    formData.to.data = data;
    update();
  }
};

function getDirection(type) {
  let parName = type + 'Name'
    , tmpIdent = path[type], tmpName = parGet(parName)
    , code = parGet(type)
    , ident = (tmpIdent ? tmpIdent : (tmpName ? translateBoth(tmpName) : null))
    , name = (tmpName ? tmpName : (tmpIdent ? capitalize(translateBoth(tmpIdent, true)) : null));

  return {
    ident : ident,
    name  : name,
    code  : code,
    data  : ST.getStation(name, code, ident, LoadedStations[type])
  };
}

function update() {
  // console.log('update', formData);

  functions.forEach(function (func) {
    func();
  });
}

function initData() {
  formData.dateFromUrl = parGet('date');
  formData.date = parseDate.parse(formData.dateFromUrl);

  formData.from = getDirection('from');
  formData.to = getDirection('to');

  update();
}

function checkUrl(event) {

  // console.log('URLParser checkURL');

  let newState = window.location.pathname + window.location.search;
  if (newState == lastState)
    return;

  lastState = newState;

  // console.clear();

  parseParameters.parse();
  path = parseCityPath.get();

  pathAdd = window.location.pathname;

  if (path.from && path.to) {
    pathAdd = (pathAdd.slice(0, pathAdd.indexOf(path.from)));
  } else {
    pathAdd = pathAdd.replace(/\/train$/, '/');
  }

  pathAdd += '/';
  pathAdd = pathAdd.replace(/([\/]{2,})/gm, '/');

  initData();

  // console.log(formData);
}

window.addEventListener('popstate', checkUrl);
window.addEventListener('wlRouterDidChange', checkUrl);

checkUrl();

const URL = {
  whenUpdate(callback){
    functions.push(callback);
  },
  getData(){
    return formData;
  },
  /**
   *
   * @param {MyOttStation} from
   * @param {MyOttStation} to
   * @param {Date} date
   */
  setUrl(from, to, date){

    let fromStr = from.url
      , toStr = to.url
      , dateStr = parseDate.stringify(date);

    let url = _config.urlRedirect + fromStr + '_' + toStr + '/?date=' + dateStr + '&s'
      , urlAbsolute = pathAdd + fromStr + '_' + toStr + '/?date=' + dateStr + '&s';

    if (window.reduxPushState) {

      window.reduxPushState(url);

    } else if (history && history.pushState) {

      history.pushState({
        from : from,
        to   : to,
        date : date
      }, '', urlAbsolute);

      checkUrl();

    } else {

      window.location = urlAbsolute;

    }
  }
};

// window.addEventListener('wlSearchResize', function(){});

export default URL;