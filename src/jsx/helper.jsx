"use strict";

import React from 'react';

import state from './../js/state';

/**
 *
 * @param event
 * @param {int} defaultIndex
 * @returns {int}
 */
function getIndexFromTarget(event, defaultIndex = 0) {
  if (event) {
    let target = event.target;

    while (target && target.tagName != 'A') {
      target = target.parentNode;
    }
    if (target.hasAttribute('data-index'))
      return parseInt(target.getAttribute('data-index'), 10);
  }
  return defaultIndex;
}

class Helper extends React.Component {
  constructor(props) {
    super(props);

    this._current = 0;

    this.state = {
      selected : 0,
      items    : props.items
    }
  }

  componentDidMount() {
    this.__setParentPlaceholder();
    // this.props.parent.setPlaceholder(this.getFocusedStr());
  }

  componentDidUpdate() {
    this.__setParentPlaceholder();
    // this.props.parent.setPlaceholder(this.getFocusedStr());
  }

  componentWillReceiveProps(nextProps) {
    this._current = 0;
    state(this, {
      selected : 0,
      items    : nextProps.items
    });
  }

  /**
   *
   * @param {int} index
   */
  setSelected(index) {
    /**
     *
     * @type {MyOttStation}
     * @private
     */
    this._current = index;
    state(this, {selected : index});
  }

  onHover(event) {
    var index = getIndexFromTarget(event, this.state.selected);
    this.setSelected(index);
  }

  /**
   *
   * @private
   */
  __setParentPlaceholder() {
    this.props.parent.setPrepareData(this._current);
  }

  down() {
    var index = this.state.selected + 1;
    if (index >= this.state.items.length)
      index = 0;
    this.setSelected(index);
  }

  up() {
    var index = this.state.selected - 1;
    if (index < 0)
      index = this.state.items.length - 1;
    this.setSelected(index);
  }

  select(event) {

    var index = getIndexFromTarget(event, this.state.selected);
    this.props.parent.setData(index);
  }

  render() {

    // if (this._items.length <= 1)
    //   return (<div></div>);

    var list = [];
    var self = this;

    this.state.items.forEach(function (station, index) {

      var selected = (index == self.state.selected);
      list.push(
        <li className="rzhd-helper__list__item" key={"item" + index}>
          <a href="javascript:void(0)"

             onMouseOver={self.onHover.bind(self)}
             onClick={self.select.bind(self)}
             data-index={index}

             tabIndex="-1"
             className={"rzhd-helper__link" + (selected ? ' focus' : '')}>
                <span><span className="city">{station.name.ru},</span><span
                  className="country"> {station.country.ru}</span></span>
          </a>
        </li>
      );

    });

    return (
      <div className="rzhd-helper">
        <ul className="rzhd-helper__list">
          {list}
        </ul>
      </div>
    );
  }
}

export default Helper;