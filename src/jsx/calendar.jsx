import React from 'react';
import Pikaday from 'pikaday';
import Placeholder from './placeholder';

import state from './../js/state';
import parseDate from './../js/parseDateFromGet';

const monthInField = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
const i18n = {
  previousMonth : 'Предыдущий месяц',
  nextMonth     : 'Следующий месяц',
  months        : ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
  weekdays      : ['Воскресенье', 'Понедельник', 'Ворник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
  weekdaysShort : ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ']
};

const currentYear = (new Date()).getFullYear();

/**
 *
 * @param {Date} date
 * @returns {string}
 */
function makeUrlStringFromDate(date) {
  if (!date)
    return '';
  let day = date.getDate()
    , month = date.getMonth() + 1
    , year = date.getFullYear();

  day = day < 10 ? "0" + day : day;
  month = month < 10 ? "0" + month : month;

  return day + '' + month + '' + year;
}
/**
 *
 * @param {Date} date
 * @returns {string}
 */
function makeValueStringFromDate(date) {
  let day = date.getDate()
    , month = date.getMonth()
    , year = date.getFullYear();

  let tmp = day + " " + monthInField[month];
  if (year != currentYear)
    tmp += ', ' + year + ' г.';

  return tmp;
}

function parseDateFromGet(dateString) {

  let date = parseDate(dateString);

  if (date)
    return {
      text : makeValueStringFromDate(date),
      date : date
    };

  return {};
}

class Calendar extends React.Component {
  constructor(props) {
    super(props);

    let tmp = parseDateFromGet(props.value);

    this.state = {
      date   : (tmp.date ? tmp.date : ''),
      helper : false,
      value  : (tmp.text ? tmp.text : '')
    }
  }

  clear() {
    this.__hideHelper();
  }

  componentDidMount() {

    this.picker = new Pikaday({
      minDate  : new Date(),
      firstDay : 1,
      i18n     : i18n,
      onSelect : this.selectDate.bind(this)
    });

    this.refs.pikaday.appendChild(this.picker.el);

    if (this.state.date) {
      this.picker.setDate(this.state.date);
    }

    this.picker.hide();
  }

  componentWillReceiveProps(newProps) {
    if (newProps.value !== this.props.value) {

      let tmp = parseDateFromGet(newProps.value);

      state(this, {
        date   : (tmp.date ? tmp.date : ''),
        helper : false,
        value  : (tmp.text ? tmp.text : '')
      });

      if (tmp.date) {
        this.picker.setDate(tmp.date);
      }

    }
  }

  touchStart(event) {
    console.log(event);
    if (event.target.classList.contains('pika-button')) {
      this._close = true;
    }
  }

  clickPikaDay(event) {
    if (event.target.classList.contains('pika-button')) {
      this.props.form.next(this);
    }
  }

  selectDate(date) {
    state(this, {
      error : false,
      date  : date,
      value : makeValueStringFromDate(date)
    });
    if (this._close) {
      this.props.form.next(this);
      this._close = false;
    }
  }

  whenChange(event) {

  }

  keyDown(event) {
    switch (event.keyCode) {
      //tab 9
      case 9:
        this.clear();
        break;
      // enter
      case 13:
        this.clear();
        event.preventDefault();
        event.stopPropagation();
        this.props.form.next(this);
        break;
    }
  }

  onClick() {
    // this.props.form.clear(this);
    this.focus();
  }

  onFocus() {
    // this.props.form.clear(this);
    this.__showHelper();
  }

  __showHelper() {
    var state = this.state;
    state.helper = true;
    this.setState(state);
    this.picker.show();
  }

  __hideHelper() {
    var state = this.state;
    state.helper = false;
    this.setState(state);
    this.picker.hide();
  }

  onBlur() {
    this.clear();
  }

  focus() {
    this.refs.input.focus();
  }

  getData() {
    return this.state.date;
  }

  getUrlPart() {
    let ret = '';
    if (this.state.date) {
      ret = makeUrlStringFromDate(this.state.date);
    }

    if (ret)
      return ret;

    this.error();

    return null;
  }

  error() {
    state(this, {error : true})
  }

  render() {
    return (
      <div className={"rzhd-form-calendar rzhd-control" + (this.state.error ? ' error' : '')}>
        <div className="rzhd-form-calendar__label rzhd-control__label">
          <div className="rzhd-control__label__placeholder">
            <Placeholder ref="placeholder" init={this.props.placeholder} value={this.state.value}/>
            <span className="rzhd-control__label__placeholder__icon"/>
          </div>
          <input type="hidden" name="date" value={makeUrlStringFromDate(this.state.date)}/>
          <input type="button"
                 autoComplete="off"
                 ref="input"
                 value={this.state.value}
                 onChange={this.whenChange.bind(this)}
                 onClick={this.onClick.bind(this)}
                 onFocus={this.onFocus.bind(this)}
                 onBlur={this.onBlur.bind(this)}
                 onKeyDown={this.keyDown.bind(this)}
                 className="rzhd-input"
                 name={this.props.name}/>
        </div>
        <div className={"rzhd-form-calendar__helper rzhd-helper-container" + (this.state.helper ? '' : ' hide')}>
          <div ref="pikaday" className="rzhd-pikaday" onTouchStart={this.touchStart.bind(this)}
               onMouseUp={this.clickPikaDay.bind(this)}/>
        </div>
      </div>
    )
  }
}

export default Calendar;