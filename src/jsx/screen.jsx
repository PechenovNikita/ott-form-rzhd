"use strict";

import React from 'react';
import state from './../js/state';

import parseDate from './../js/parseDateFromGet';
import STData from './../js/getStation';

import translateBoth from './../js/assets/translateBoth';

const currentYear = (new Date()).getFullYear();

const monthInField = ['янв', 'февр', 'марта', 'апр', 'мая', 'июня', 'июля', 'авг', 'сен', 'окт', 'нояб', 'дек'];

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 *
 * @param {Date} date
 * @returns {string}
 */
function makeValueStringFromDate(date) {
  if (date) {
    let day = date.getDate()
      , month = date.getMonth()
      , year = date.getFullYear();

    let tmp = day + " " + monthInField[month];
    if (year != currentYear)
      tmp += ', ' + year + ' г.';

    return tmp;
  }
  return '';
}

/**
 *
 * @param {Object} props
 * @param {function} fromCallback
 * @param {function} toCallback
 * @returns {{date: *, from: *, to: *}}
 */
function getProps(props, fromCallback, toCallback) {
  let from = null;

  if (props.data) {
    if (props.data.fromName) {
      from = props.data.fromName;
    } else if (props.data.state && props.data.state.from) {
      from = props.data.state.from;
    } else if (props.data.fromID && props.data.fromID.length > 0) {
      from = (STData.getStationByName(props.data.fromID, fromCallback));
    } else if (props.data.from && props.data.from.length > 0) {
      from = (STData.getStationByCode(props.data.from, fromCallback));
    }
  }
  if (from && from.toString() !== from)
    from = (from.name && from.name.ru ? from.name.ru : '');

  let to = null;

  if (props.data) {
    if (props.data.toName) {
      to = props.data.toName;
    } else if (props.data.state && props.data.state.to) {
      to = props.data.state.to;
    } else if (props.data.toID && props.data.toID.length > 0) {
      to = (STData.getStationByName(props.data.toID, toCallback));
    } else if (props.data.to && props.data.to.length > 0) {
      to = (STData.getStationByCode(props.data.to, toCallback));
    }
  }

  if (to && to.toString() !== to)
    to = (to.name && to.name.ru ? to.name.ru : '');

  let date = null;

  if (props.state && props.state.date)
    date = props.state.date;
  else if (props.data)
    date = parseDate(props.data.date);

  return {
    date : date,
    from : from,
    to   : to,

    fromTranslate : props.data.fromID,
    toTranslate   : props.data.toID
  };
}

class Screen extends React.Component {

  constructor(props) {
    super(props);
    this.state = getProps(props, this.updateDataFrom.bind(this), this.updateDataTo.bind(this));
  }

  componentWillReceiveProps(newProps) {
    state(this, getProps(newProps, this.updateDataFrom.bind(this), this.updateDataTo.bind(this)));
  }

  updateDataFrom(data) {
    state(this, {
      from : (data.name && data.name.ru ? data.name.ru : ''),
    });
  }

  updateDataTo(data) {
    state(this, {
      to : (data.name && data.name.ru ? data.name.ru : ''),
    });
  }

  editClick(event) {
    event.stopPropagation();
    event.preventDefault();

    this.props.parent.edit();
  }

  render() {
    let from = this.state.from
      , to = this.state.to
      , date = makeValueStringFromDate(this.state.date);

    let text = (<span>Маршрут не выбран</span>);

    if (from && to && date) {
      text = (<span>{from} — {to}, {date}</span>);
    } else if (date && this.state.fromTranslate && this.state.toTranslate) {
      text = (<span>{capitalizeFirstLetter(translateBoth(this.state.fromTranslate, true))} — {capitalizeFirstLetter(translateBoth(this.state.toTranslate, true))}, {date}</span>);
    } else {
      this.props.parent.edit();
    }

    return (
      <div className="block-rzhd-search__screen__line" onClick={this.editClick.bind(this)}>
        {text}
        <button className="rzhd-edit-icon"/>
      </div>
    );
  }
}

export default Screen;