import React from 'react';
import Station from './station';
import JSONP from './../js/JSONP';
import Calendar from './calendar';

import state from './../js/state';
import _config from './../js/_config';
import STData from './../js/getStation';
import URLParser from './../js/urlData';

class Form extends React.Component {

  constructor(props) {
    super(props);

    this.values = {
      from     : '',
      to       : '',
      calendar : ''
    }
    this.state = {
      reverse : false,
      initial : props.data
    }

  }

  componentWillReceiveProps(newProps) {
    if (newProps.data !== this.state.initial) {
      state(this, {initial : newProps.data});
    }
    if (this.props.update !== newProps.update)
      this.resize();
  }

  clear(element) {
    if (element != this.refs.from)
      this.refs.from.clear();
    if (element != this.refs.to)
      this.refs.to.clear();
    if (element != this.refs.calendar)
      this.refs.calendar.clear();
  }

  componentDidMount() {
    this.resize();

    window.addEventListener('resize', this.resize.bind(this));
    document.addEventListener('focus', this.focus.bind(this));
  }

  focus(event) {
    var target = event.target;

    if (target && target.parentNode) {
      if (target.classList.contains('rzhd-form')) {
        this.clear();
        return;
      }
      target = target.parentNode;
    }
  }

  resize() {

    this.__clearAllSize();

    var width = this.refs.container.offsetWidth;
    var inp_w = Math.floor((width - 102 - 14 * 4) / 3)
      , cal_w = inp_w, btn_w = 102;
    var left = false;
    if (inp_w < 250) {
      inp_w = Math.floor((width - 14 * 2) / 2);
      cal_w = inp_w * 1.2;
      left = true;
      if (inp_w < 250) {
        inp_w = Math.floor(width - 14);
        cal_w = inp_w;
      }

      // this.setState({reverse : true});
    } else {
      // this.setState({reverse : false});
    }
    var state = this.state;
    state.left = left;
    this.setState(state);
    this.__setSizeBtn(btn_w + 'px').__setSizeInputs(inp_w + 'px').__setSizeCalendar(cal_w + 'px');
  }

  __clearAllSize() {
    return this.__setSizeBtn('').__setSizeCalendar('').__setSizeInputs('');
  }

  __setSizeInputs(size) {
    this.refs.from_container.style.width = size;
    this.refs.to_container.style.width = size;
    return this;
  }

  __setSizeCalendar(size) {
    this.refs.calendar_container.style.width = size;
    return this;
  }

  __setSizeBtn(size) {
    this.refs.btn_container.style.width = size;
    return this;
  }

  blur(event) {
    // console.log(event.target);
  }

  next(current) {
    if (this.refs.from == current)
      this.refs.to.focus();
    else if (this.refs.to == current)
      this.refs.calendar.focus();
    else if (this.refs.calendar == current) {
      this.refs.btn.focus();
      // this.submit();
    }
  }

  submit(event) {
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    let from = this.refs.from.getUrlPart()
      , to = this.refs.to.getUrlPart()
      , calendar = this.refs.calendar.getUrlPart();

    if (from && to && calendar) {
      let url = _config.urlRedirect + from + '_' + to + '/?date=' + calendar + '&s';

      if (history && history.pushState) {

        let state = {
          from : this.refs.from.getData(),
          to   : this.refs.to.getData(),
          date : this.refs.calendar.getData()
        };

        if (window.reduxPushState) {
          window.reduxPushState(url)
        } else {
          history.pushState(state, '', url)
        }

        URLParser.pushState(state);

      } else {

        window.location = url;

      }

      this.props.parent.screen();
    }

  }

  render() {
    return (
      <form onSubmit={this.submit.bind(this)} ref="container" className="rzhd-form" onBlur={this.blur.bind(this)}>
        <div ref="from_container" className="rzhd-form__from rzhd-form__cell">
          <Station ref="from"
                   ident={this.state.initial.fromID}
                   value={this.state.initial.fromName}
                   code={this.state.initial.from}
                   form={this} name="from" placeholder="Откуда"/>
        </div>
        <div ref="to_container" className="rzhd-form__to rzhd-form__cell">
          <Station ref="to"
                   ident={this.state.initial.toID}
                   value={this.state.initial.toName}
                   code={this.state.initial.to}
                   form={this} name="to" placeholder="Куда"/>
        </div>
        <div ref="calendar_container"
             className={"rzhd-form__calendar rzhd-form__cell" + (this.state && this.state.left ? ' left' : '')}>
          <Calendar ref="calendar" value={this.state.initial.date} form={this} name="calendar"
                    placeholder="Выберите дату"/>
        </div>
        <div ref="btn_container" className="rzhd-form__submit rzhd-form__cell">
          <button ref="btn" className="rzhd-btn">
            <span className="rzhd-btn__inner">ИСКАТЬ</span>
          </button>
        </div>
      </form>
    );
  }
}

export default Form;