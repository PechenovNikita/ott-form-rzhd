"use strict";

import React from 'react';
import state from './../js/state';

class Placeholder extends React.Component {

  /**
   *
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = {
      text   : (props.value.length <= 0 ? props.init : ''),
      hidden : props.value
    };
  }

  /**
   *
   * @param {Object} newProps
   */
  componentWillReceiveProps(newProps) {
    state(this, {hidden : newProps.value})
  }

  /**
   *
   * @param {MyOttStation} data
   * @param {boolean} stayOld
   * @returns {*}
   */
  station(data, stayOld) {
    let name = (data && data.name && data.name.ru ? data.name.ru : '')
      , country = (name && data && data.country && data.country.ru ? (', ' + data.country.ru) : '')
      , str = name + country;
    this.setBackText(str, !!stayOld);
  }

  /**
   *
   * @param {string} text
   * @param {boolean=} stayOld
   */
  setBackText(text, stayOld) {
    // console.log('setBackText', arguments);
    if ((!text || text.length <= 0) && stayOld)
      state(this, {text : this.state.text});
    else {
      state(this, {text : text});
    }
  }

  /**
   *
   */
  clearText() {
    state(this, {text : ''});
  }

  /**
   *
   * @returns {XML}
   */
  render() {
    var placeholder = this.state.text;

    if (placeholder && this.state.hidden && this.state.hidden.length > 0) {
      if (this.state.hidden.length < 2)
        placeholder = '';
      else {
        if (placeholder.length > this.state.hidden.length && placeholder != this.props.init) {
          placeholder = (
            <span className="back"><span
              className="hidden">{this.state.hidden}</span>{placeholder.slice(this.state.hidden.length)}</span>);
        } else
          placeholder = '';
      }
    } else if (!this.state.hidden || this.state.hidden.length <= 0) {
      placeholder = this.props.init;
    }


    return (
      <span className="rzhd-control__label__placeholder__inner">{placeholder}</span>
    );
  }
}

export default Placeholder;