"use strict";

import React from 'react';
import JSONP from './../js/JSONP';
import Helper from './helper';
import Placeholder from './placeholder';
import STData from './../js/getStation';

import state from './../js/state';

var storageAjax = {};
var storageRequestText = {};

class Station extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      value : props.value,
      data  : null,
      items : []
    };

    let data = STData.getStationByAll(props.value, props.code, props.ident, this.updateData.bind(this));

    // if (props.ident && props.ident.length > 0) {
    //   data = (STData.getStationByName(props.ident, this.updateData.bind(this)));
    // } else if (props.code) {
    //   data = (STData.getStationByCode(props.code, this.updateData.bind(this)));
    // }

    if (data) {
      this.state.value = (data.name && data.name.ru ? data.name.ru : '');
      this.state.data = data;
    }

    this._value = '';
  }

  componentWillReceiveProps(newProps) {

    let data = STData.getStationByAll(newProps.value, newProps.code, newProps.ident, this.updateData.bind(this));

    if (data) {
      state(this, {
        value : (data.name && data.name.ru ? data.name.ru : ''),
        data  : data
      });

      this.selected = data;
      this.setPlaceholderFromData(data);
    } else {
      state(this, {
        value : '',
        data  : null
      });
      this.selected = null;
      this.__setPlaceholder('');
    }

    // if (newProps.data !== this.state.initial) {
    //   state(this, {initial : newProps.data});
    // }
  }

  updateData(data) {

    state(this, {
      value : (data.name && data.name.ru ? data.name.ru : ''),
      data  : data
    });

    this.selected = data;
    this.setPlaceholderFromData(data);
  }

  whenChange(event) {

    var inputText = event.target.value;
    this._value = inputText;

    if (inputText.length >= 2) {
      if (storageAjax[inputText]) {
        this.request = 1;
        this.getResponse(storageAjax[inputText], 1);
      } else {
        this.request = JSONP.station(this.getResponse.bind(this), inputText);
        storageRequestText[this.request] = inputText;
      }
      this.__setPlaceholder('', true);

    } else {
      this
        .clearHelper()
        .__setPlaceholder();
    }

    state(this, {
      value : inputText,
      error : false,
      data  : null
    });

    this.selected = null;
  }

  __setPlaceholder(text, stayOld) {
    this.refs.placeholder.setBackText(text, stayOld);
    return this;
  }

  onBlur() {
    this._blurred = true;
    setTimeout(this.__blur.bind(this), 10);
  }

  __blur() {
    if (this._blurred) {
      this._blurred = false;

      if (!this.selected && !this.state.data && this.state.items.length > 0) {
        this.setData(0);
      } else
        this.clear();

    }
  }

  onFocus() {
    // this.props.form.clear(this);
  }

  keyDown(event) {
    // console.log(event.keyCode);

    switch (event.keyCode) {
      // down 40
      case 40:
        event.preventDefault();
        event.stopPropagation();

        this.refs.helper.down();
        break;
      // up 38
      case 38:
        event.preventDefault();
        event.stopPropagation();

        this.refs.helper.up();
        break;
      // enter 13
      case 13:
        event.preventDefault();
        event.stopPropagation();

        this.setData();

        break;
      // tab 9
      case 9:
        // event.preventDefault();
        // event.stopPropagation();
        this.setData(undefined, true);

        break;
    }
  }

  /**
   *
   * @param {AJAXOttResults} response
   * @param {number} id
   */
  getResponse(response, id) {
    var inputText = storageRequestText[id];
    storageAjax[inputText] = response;

    if (!this._value || this._value.length < 0 || (this.request && this.request !== id))
      return;

    if (response.success) {
      this.setHelper(response.result);
    } else {
      this.clearHelper();
    }
  }

  setPrepareData(index) {
    this._prepare = index;
    this.setPlaceholderFromData(this.state.items[index])
  }

  setPlaceholderFromData(data) {
    this.refs.placeholder.station(data);
  }

  /**
   *
   * @param {Array<AJAXOttCity>} results
   * @returns {Station}
   */
  setHelper(results) {
    let items = [];
    if (STData.loaded()) {
      items = results.reduce(function (arr, el, index) {
        /**
         *
         * @type {?MyOttStation}
         */
        var temp = STData.getStation(el.id);

        if (temp) {
          arr.push(temp);
        }

        return arr;
      }, []);
    } else {
      items = [];
    }

    state(this, {items : items});

    if (items.length <= 0)
      this.__setPlaceholder('');

    return this;
  }

  clearHelper() {
    state(this, {items : []});
    return this;
  }

  clear() {
    this.clearHelper();
  }

  setData(index = this._prepare, notNext = false) {
    if (this.selected && !this.state.items[index])
      return notNext ? this : this.props.form.next(this);
    this.selected = this.state.items[index];

    if (!this.selected)
      return;
    this.setPlaceholderFromData(this.selected);
    state(this, {
      value : this.selected.name.ru,
      data  : this.selected,
      items : []
    });
    return notNext ? this : this.props.form.next(this);
  }

  getData() {
    return this.selected;
  }

  getUrlPart() {
    if (this.selected) {
      return this.selected.url;
    }
    if (this._blurred) {
      this.__blur();
      return this.getUrlPart();
    }
    this.error();
    return null;
  }

  focus() {
    this.refs.input.focus();
  }

  error() {
    state(this, {error : true});
  }

  render() {
    let helper = (!this.state.items || this.state.items.length <= 0) ? '' :
      <Helper ref="helper" items={this.state.items} parent={this}/>;

    return (
      <div className={"rzhd-form-station rzhd-control" + (this.state.error ? ' error' : '')}>
        <div className="rzhd-form-station__label rzhd-control__label">
          <div className="rzhd-control__label__placeholder">
            <Placeholder ref="placeholder" init={this.props.placeholder} value={this.state.value}/>
          </div>
          <input type="hidden" name={this.props.name}
                 value={(this.state.data && this.state.data.code ? this.state.data.code : '')}/>

          <input type="text"
                 ref="input"
                 value={this.state.value}
                 autoComplete="off"
                 onKeyDown={this.keyDown.bind(this)}
                 onChange={this.whenChange.bind(this)}
                 onBlur={this.onBlur.bind(this)}
                 onFocus={this.onFocus.bind(this)}
                 className="rzhd-input"
                 name={this.props.name + "Name"}/>
        </div>
        <div className="rzhd-form-station__helper rzhd-helper-container">
          {helper}
        </div>
      </div>
    );
  }
}

export default Station;