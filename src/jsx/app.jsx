"use strict";

import React from 'react';
import ReactDOM from 'react-dom';

import state from './../js/state';
import Form from './form';
import Screen from './screen';
import URLParser from './../js/urlData';

function getDataFromUrl() {

  let path = URLParser.parseCityPath();

  return {
    date : URLParser.getParameter('date'),

    fromID   : path.from,
    fromName : URLParser.getParameter('fromName', ''),
    from     : parseInt(URLParser.getParameter('from'), 10),
    metaFrom : parseInt(URLParser.getParameter('metaFrom'), 10),

    toID   : path.to,
    toName : URLParser.getParameter('toName', ''),
    to     : parseInt(URLParser.getParameter('to'), 10),
    metaTo : parseInt(URLParser.getParameter('metaTo'), 10),

    state : URLParser.getState()
  };
}

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      form : false,
      data : getDataFromUrl()
    };

    this.initUrlChange();
  }

  initUrlChange() {
    URLParser.whenChange(this.updateFromUrl.bind(this));
  }

  updateFromUrl() {
    state(this, {data : getDataFromUrl()});
  }

  edit() {
    if (this.state.form)
      return;

    if (!this.refs.wrapForm || !this.refs.wrapScreen) {
      state(this, {
        form : true,
      });
      return;
    }

    let self = this;

    state(this, {
      form   : true,
      fade   : true,
      height : this.refs.wrapScreen.offsetHeight
    });

    setTimeout(function () {
      state(self, {
        height : self.refs.wrapForm.offsetHeight
      });

    }, 10);

    setTimeout(function () {
      state(self, {
        fade   : false,
        height : ''
      })
    }, 500);
  }

  screen() {
    if (!this.state.form)
      return;

    if (!this.refs.wrapForm || !this.refs.wrapScreen) {
      state(this, {
        form : false,
      });
      return;
    }

    let self = this;

    state(this, {
      form   : false,
      fade   : true,
      height : this.refs.wrapForm.offsetHeight
    });

    setTimeout(function () {
      state(self, {
        height : self.refs.wrapScreen.offsetHeight
      });

    }, 10);

    setTimeout(function () {
      state(self, {
        fade   : false,
        height : ''
      })
    }, 500);
  }


  render() {
    return (
      <div className="rzhd-wrapper" style={{height : (this.state.height ? this.state.height : 'auto')}}>

        <div ref="wrapForm"
             className={"block-rzhd-search__form" + (!this.state.form ? (this.state.fade ? ' rzhd-fade' : '') + ' rzhd-hide' : '')}>
          <div className="block-rzhd-search__form__title">
            <h2 className="block-rzhd-search__form__title__inner">Поиск ж/д билетов</h2>
          </div>
          <div className="block-rzhd-search__form__inner">

            <Form ref="form" parent={this} data={this.state.data} update={this.state.form}/>

          </div>
        </div>

        <div ref="wrapScreen"
             className={"block-rzhd-search__screen" + (this.state.form ? (this.state.fade ? ' rzhd-fade' : '') + ' rzhd-hide' : '')}>
          <div className="block-rzhd-search__screen__title">
            <span className="block-rzhd-search__screen__title__inner">Поезда</span>
          </div>
          <Screen data={this.state.data} parent={this}/>
        </div>

      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('rzhd_form'));

export default App;