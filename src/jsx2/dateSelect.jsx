import React from 'react';
import Pikaday from 'pikaday';
import Placeholder from './placeholder';

import state from './../js/state';
import parseDate from './../js/mixin/url/parseDate';

const i18n = {
  previousMonth : 'Предыдущий месяц',
  nextMonth     : 'Следующий месяц',
  months        : ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
  weekdays      : ['Воскресенье', 'Понедельник', 'Ворник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
  weekdaysShort : ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ']
};



// const currentYear = (new Date()).getFullYear();

class dateSelect extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      date   : props.value,
      helper : false
    }
  }

  clear() {
    this.__hideHelper();
  }

  componentDidMount() {

    let currentDate = new Date();

    this.picker = new Pikaday({
      minDate  : currentDate,
      maxDate  : new Date((new Date()).setDate(currentDate.getDate() + 60)),
      firstDay : 1,
      i18n     : i18n,
      onSelect : this.selectDate.bind(this)
    });


    this.refs.pikaday.appendChild(this.picker.el);

    if (this.state.date) {
      this.picker.setDate(this.state.date);
    }

    this.picker.hide();
  }

  componentWillReceiveProps(newProps) {
    if (newProps.value !== this.props.value) {

      state(this, {
        date : newProps.value,
      });

      if (newProps.value) {
        this.picker.setDate(newProps.value);
      }

    }
  }

  touchStart(event) {
    if (event.target.classList.contains('pika-button')) {
      this._close = true;
    }
  }

  clickPikaDay(event) {
    if (event.target.classList.contains('pika-button')) {
      this.props.form.next(this);
    }
  }

  selectDate(date) {

    state(this, {
      error : false,
      date  : date
    });

    if (this._close) {
      this._close = false;
      this.props.form.next(this);
    }
  }

  keyDown(event) {
    switch (event.keyCode) {
      // //tab 9
      // case 9:
      //   this.clear();
      //   break;
      // enter
      case 13:
        console.log('13');
        event.preventDefault();
        event.stopPropagation();
        // this.clear();
        this.props.form.next(this);
        break;
    }
  }

  onClick() {
    // this.props.form.clear(this);
    this.focus();
  }

  onFocus() {
    // this.props.form.clear(this);
    this.__showHelper();
  }

  __showHelper() {
    state(this, {helper : true});
    this.picker.show();
  }

  __hideHelper() {
    state(this, {helper : false});
    this.picker.hide();
  }

  onBlur() {
    this.clear();
  }

  focus() {
    this.refs.input.focus();
  }

  getData() {
    if (this.state.date) {
      return this.state.date;
    }
    this.__setError();
    return null;

  }

  __setError() {
    state(this, {error : true})
  }

  render() {
    var value = parseDate.stringifyDateSelect(this.state.date);

    return (
      <div className={"rzhd-form-calendar rzhd-control" + (this.state.error ? ' error' : '')}>
        <div className="rzhd-form-calendar__label rzhd-control__label">
          <div className="rzhd-control__label__placeholder">
            <Placeholder ref="placeholder" init={this.props.placeholder} value={value} back=""/>
            <span className="rzhd-control__label__placeholder__icon"/>
          </div>
          <input type="button"
                 autoComplete="off"
                 ref="input"
                 value={value}

                 onClick={this.onClick.bind(this)}
                 onFocus={this.onFocus.bind(this)}
                 onBlur={this.onBlur.bind(this)}
                 onKeyDown={this.keyDown.bind(this)}

                 className="rzhd-input"
                 name={this.props.name}/>
        </div>
        <div className={"rzhd-form-calendar__helper rzhd-helper-container" + (this.state.helper ? '' : ' hide')}>
          <div ref="pikaday" className="rzhd-pikaday" onTouchStart={this.touchStart.bind(this)}
               onMouseUp={this.clickPikaDay.bind(this)}/>
        </div>
      </div>
    )
  }
}

export default dateSelect;