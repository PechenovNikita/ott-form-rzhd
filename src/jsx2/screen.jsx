"use strict";

import React from 'react';
import state from './../js/state';
import parseDate from './../js/mixin/url/parseDate';

function parseOneDirection(type, props) {
  let tmp = null;

  if (props.data && props.data[type]) {
    let tmpData = props.data[type];

    if (tmpData.data) {
      tmp = (tmpData.data.name && tmpData.data.name.ru ? tmpData.data.name.ru : null);
    }

    if (!tmp && tmpData.name) {
      tmp = tmpData.name;
    }
  }

  return tmp;
}

/**
 *
 * @param {Object} props
 * @returns {{date: *, from: *, to: *}}
 */
function parseProps(props) {
  return {
    date : props.data.date,
    from : parseOneDirection('from', props),
    to   : parseOneDirection('to', props)
  };
}

class Screen extends React.Component {

  constructor(props) {
    super(props);
    this.state = parseProps(props);
  }

  componentWillReceiveProps(newProps) {
    state(this, parseProps(newProps))
  }

  editClick(event) {
    event.stopPropagation();
    event.preventDefault();

    this.props.app.edit();
  }

  render() {
    let from = this.state.from
      , to = this.state.to
      , date = parseDate.stringifyScreen(this.state.date);

    let text = (<span>Маршрут не выбран</span>);

    if (from && to && date) {
      text = (<span>{from} — {to}, {date}</span>);
    }

    return (
      <div className="block-rzhd-search__screen__line" onClick={this.editClick.bind(this)}>
        {text}&nbsp;<button className="rzhd-edit-icon"/>
      </div>
    );
  }
}

export default Screen;