"use strict";

import React from 'react';

import state from './../js/state';

/**
 *
 * @param event
 * @param {int} defaultIndex
 * @returns {int}
 */
function getIndexFromTarget(event, defaultIndex = 0) {
  if (event) {
    let target = event.target;

    while (target && target.tagName != 'A') {
      target = target.parentNode;
    }
    if (target.hasAttribute('data-index'))
      return parseInt(target.getAttribute('data-index'), 10);
  }
  return defaultIndex;
}

class Helper extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selected : 0,
      items    : props.items
    }
  }

  componentWillReceiveProps(nextProps) {

    if (this.state.items === nextProps.items)
      return;

    state(this, {
      selected : 0,
      items    : nextProps.items
    });
  }

  /**
   *
   * @param {int} index
   */
  __setSelected(index) {
    state(this, {selected : index});
    this.props.parent.setPlaceHolder(index);
  }

  down() {
    var index = this.state.selected + 1;
    if (index >= this.state.items.length)
      index = 0;
    this.__setSelected(index);
  }

  up() {
    var index = this.state.selected - 1;
    if (index < 0)
      index = this.state.items.length - 1;
    this.__setSelected(index);
  }

  __selectFromEvent(event) {
    this.__setSelected(getIndexFromTarget(event, this.state.selected));
  }

  onHover(event) {
    this.__selectFromEvent(event);
  }

  onSelect(event) {
    event.stopPropagation();
    event.preventDefault();

    this.__selectFromEvent(event);
    this.props.parent.setData(true);
  }

  render() {

    // if (this._items.length <= 1)
    //   return (<div></div>);

    var list = [];
    var self = this;

    this.state.items.forEach(function (station, index) {

      let selected = (index == self.state.selected);
      list.push(
        <li className="rzhd-helper__list__item" key={"item" + index}>
          <a href="javascript:void(0)"

             onMouseOver={self.onHover.bind(self)}
             onClick={self.onSelect.bind(self)}
             onTouchStart={self.onSelect.bind(self)}
             data-index={index}

             tabIndex="-1"
             className={"rzhd-helper__link" + (selected ? ' focus' : '')}>
                <span><span className="city">{station.name.ru},</span><span
                  className="country"> {station.country.ru}</span></span>
          </a>
        </li>
      );

    });

    return (
      <div className="rzhd-helper">
        <ul className="rzhd-helper__list">
          {list}
        </ul>
      </div>
    );
  }
}

export default Helper;