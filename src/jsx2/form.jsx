import React from 'react';
import Station from './station';
// import JSONP from './../js/JSONP';
import DateSelect from './dateSelect';

import state from './../js/state';
import App from './app';

// import _config from './../js/_config';
// import STData from './../js/getStation';
// import URLParser from './../js/urlData';

class Form extends React.Component {

  constructor(props) {
    super(props);

    this.update = true;

    this.state = {
      reverse : false,

      from : props.data.from,
      to : props.data.to,
      date : props.data.date
    }

  }

  componentWillReceiveProps(newProps) {
    // console.log('Form componentWillReceiveProps', newProps);
    this.update = true;
    state(this, {
      from : newProps.data.from,
      to   : newProps.data.to,
      date : newProps.data.date
    });

  }

  componentDidMount() {

    this._container = this.refs.container;
    while (!this._container.classList.contains('rzhd-wrapper')) {
      this._container = this._container.parentNode;
    }
    this.resize();

    window.addEventListener('resize', this.resize.bind(this));
    // document.addEventListener('focus', this.focus.bind(this));
  }

  clear(element) {
    if (element != this.refs.from)
      this.refs.from.clear();
    if (element != this.refs.to)
      this.refs.to.clear();
    if (element != this.refs.dateSelect)
      this.refs.dateSelect.clear();
  }

  focus(event) {
    // var target = event.target;
    //
    // while (target && target.parentNode) {
    //   if (target.classList.contains('rzhd-form')) {
    //     this.clear();
    //     return;
    //   }
    //   target = target.parentNode;
    // }
  }

  resize() {

    this.__clearAllSize();

    let width = this._container.offsetWidth;
    let inp_w = Math.floor((width - 102 - 14 * 4) / 3)
      , cal_w = inp_w, btn_w = 102;
    let left = false;

    if (width < 483) {
      inp_w = Math.floor(width - 14);
      cal_w = inp_w;
      btn_w = inp_w - 20;
    }

    // if (inp_w < 250) {
    // inp_w = Math.floor((width - 14 * 2) / 2);
    // cal_w = inp_w * 1.2;
    // left = true;
    //
    // if (inp_w < 250) {
    //   inp_w = Math.floor(width - 14);
    //   cal_w = inp_w;
    // }

    // this.setState({reverse : true});
    // } else {
    // this.setState({reverse : false});
    // }

    state(this, {
      left   : left
    });

    this.__setSizeBtn(btn_w + 'px').__setSizeInputs(inp_w + 'px').__setSizeDateSelect(cal_w + 'px');
  }

  __clearAllSize() {
    return this.__setSizeBtn('').__setSizeDateSelect('').__setSizeInputs('');
  }

  __setSizeInputs(size) {
    this.refs.from_container.style.width = size;
    this.refs.to_container.style.width = size;
    return this;
  }

  __setSizeDateSelect(size) {
    this.refs.dateSelect_container.style.width = size;
    return this;
  }

  __setSizeBtn(size) {
    this.refs.btn_container.style.width = size;
    return this;
  }

  // blur(event) {
  // console.log('Form blur');
  // console.log(event.target);
  // }

  next(current) {
    // console.log('next', current);
    if (this.refs.from == current)
      this.refs.to.focus();
    else if (this.refs.to == current)
      this.refs.dateSelect.focus();
    else if (this.refs.dateSelect == current) {
      this.refs.btn.focus();
      // this.submit();
    }
  }

  submit(event) {

    // console.log('Form submit');

    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }

    let from = this.refs.from.getData()
      , to = this.refs.to.getData()
      , date = this.refs.dateSelect.getData();

    if (from && to && date) {
      App.submit(from, to, date);
    }

  }

  render() {
    let self = this;

    setTimeout(function () {
      self.update = false;
    }, 10);

    return (
      <form onSubmit={this.submit.bind(this)} ref="container" className="rzhd-form">
        <div ref="from_container" className="rzhd-form__from rzhd-form__cell">

          <Station ref="from" form={this} name="from" placeholder="Откуда" data={this.state.from} update={this.update}/>

        </div>
        <div ref="to_container" className="rzhd-form__to rzhd-form__cell">

          <Station ref="to" form={this} name="to" placeholder="Куда" data={this.state.to} update={this.update}/>

        </div>
        <div ref="dateSelect_container"
             className={"rzhd-form__calendar rzhd-form__cell" + (this.state && this.state.left ? ' left' : '')}>

          <DateSelect ref="dateSelect" value={this.state.date}
                      form={this} name="date"
                      placeholder="Выберите дату" update={this.update}/>

        </div>
        <div ref="btn_container" className="rzhd-form__submit rzhd-form__cell">
          <button ref="btn" className="rzhd-btn">
            <span className="rzhd-btn__inner">ИСКАТЬ</span>
          </button>
        </div>
      </form>
    );
  }
}

export default Form;