"use strict";

import React from 'react';
import JSONP from './../js/JSONP';
import Helper from './helper';
import Placeholder from './placeholder';

import STData from './../js/Stations';

import state from './../js/state';

var storageAjax = {};
var storageRequestText = {};

/**
 *
 * @param {StateDataStation} dataStation
 * @returns {string}
 */
function setValue(dataStation) {
  let value = '';

  if (dataStation.data && dataStation.data.name && dataStation.data.name.ru)
    value = dataStation.data.ru;
  if (!value && dataStation.name)
    value = dataStation.name;

  return (value ? value : '');
}

function setValueFromData(data) {

  let value = '';

  if (data && data.name && data.name.ru)
    value = data.name.ru;

  return (value ? value : '');
}

function getPlaceholder(data) {
  let name = (data && data.name && data.name.ru ? data.name.ru : '')
    , country = (name && data && data.country && data.country.ru ? (', ' + data.country.ru) : '');
  return name + country;
}

class Station extends React.Component {

  constructor(props) {
    super(props);

    // console.log('Station constructor', props);

    this.state = {
      error       : false,
      value       : setValue(props.data),
      data        : props.data.data,
      placeholder : props.data.data,
      items       : []
    };

  }

  componentWillReceiveProps(newProps) {
    // console.log('Station componentWillReceiveProps', newProps.update);
    if (!newProps.update)
      return;

    state(this, {
      error       : false,
      value       : setValue(newProps.data),
      data        : newProps.data.data,
      placeholder : newProps.data.data,
    });

    this._focus = false;

  }

  whenChange(event) {


    let inputText = event.target.value;


    // console.log('Station whenChange', inputText);

    if (inputText.length >= 2) {


      if (storageAjax[inputText]) {
        this.__request = 1;
        this.getResponse(storageAjax[inputText], 1);
      } else {

        this.__request = JSONP.station(this.getResponse.bind(this), inputText);
        storageRequestText[this.__request] = inputText;
      }

    } else {
      this.clear();
      state(this, {placeholder : null});
    }

    state(this, {
      value : inputText,
      error : false,
      data  : null
    });
  }

  /**
   *
   * @param {number} index
   */
  setPlaceHolder(index) {
    state(this, {placeholder : this.state.items[index]});
  }

  setData(next) {
    if (this.state.placeholder)
      state(this, {
        value : setValueFromData(this.state.placeholder),
        data  : this.state.placeholder
      });
    if (next)
      this.props.form.next(this);
  }

  onBlur() {
    this._focus = false;
    this.setData();
    setTimeout(this.__blur.bind(this), 10);
  }

  __blur() {
    // if (!this._focus) {

    this.clear();

    // if (!this.selected && !this.state.data && this.state.items.length > 0) {
    //   this.setData(0);
    // } else
    //   this.clear();

    // }
  }

  onFocus() {
    this._focus = true;
    // this.props.form.clear(this);
  }

  keyDown(event) {
    // console.log(event.keyCode);

    switch (event.keyCode) {
      // down 40
      case 40:
        event.preventDefault();
        event.stopPropagation();

        this.refs.helper.down();
        break;
      // up 38
      case 38:
        event.preventDefault();
        event.stopPropagation();

        this.refs.helper.up();
        break;
      //   // enter 13
      case 13:
        event.preventDefault();
        event.stopPropagation();

        this.setData();
        this.props.form.next(this);

        break;
      //   // tab 9
      //   case 9:
      //     // event.preventDefault();
      //     // event.stopPropagation();
      //     this.setData(undefined, true);
      //
      //     break;
    }
  }

  setPrepareData(index) {
    // this._prepare = index;
    // this.setPlaceholderFromData(this.state.items[index])
  }

  /**
   *
   * @param {AJAXOttResults} response
   * @param {number} id
   */
  getResponse(response, id) {

    if (!this._focus)
      return;

    var inputText = storageRequestText[id];
    storageAjax[inputText] = response;

    if (!this.state.value || this.state.value.length < 2 || !this.__request || this.__request !== id) {
      return;
    }

    this.__request = null;

    if (response.success) {
      this.setHelper(response.result);
    } else {
      this.clearHelper();
    }

  }

  /**
   *
   * @param {Array<AJAXOttCity>} results
   * @returns {Station}
   */
  setHelper(results) {
    let items = [];

    if (STData.loaded()) {
      items = results.reduce(function (arr, el, index) {
        /**
         *
         * @type {?MyOttStation}
         */
        var temp = STData.getStationById(el.id);

        if (temp) {
          arr.push(temp);
        }

        return arr;
      }, []);
    } else {
      items = [];
    }

    state(this, {
      items       : items,
      placeholder : items[0]
    });

    return this;
  }

  clearHelper() {
    state(this, {items : []});
    return this;
  }

  clear() {
    this.clearHelper();
  }

  getData() {
    if (this.state.data) {
      return this.state.data;
    }
    this.__setError();
    return null;

  }

  focus() {
    this.refs.input.focus();
  }

  __setError() {
    state(this, {error : true});
  }

  render() {
    let helper = (!this.state.items || this.state.items.length <= 0) ? '' :
      <Helper ref="helper" items={this.state.items} parent={this}/>;

    let value = this.state.value;

    return (
      <div className={"rzhd-form-station rzhd-control" + (this.state.error ? ' error' : '')}>
        <div className="rzhd-form-station__label rzhd-control__label">
          <div className="rzhd-control__label__placeholder">

            <Placeholder ref="placeholder"

                         parent={this}

                         init={this.props.placeholder}
                         value={this.state.value}
                         back={getPlaceholder(this.state.placeholder)}/>

          </div>
          <input type="text"
                 ref="input"
                 value={value}
                 autoComplete="off"

                 onChange={this.whenChange.bind(this)}

                 onKeyDown={this.keyDown.bind(this)}
                 onBlur={this.onBlur.bind(this)}
                 onFocus={this.onFocus.bind(this)}

                 className="rzhd-input"
                 name={this.props.name + "Name"}/>
        </div>
        <div className="rzhd-form-station__helper rzhd-helper-container">
          {helper}
        </div>
      </div>
    );
  }
}

export default Station;