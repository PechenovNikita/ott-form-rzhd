"use strict";

import React from 'react';
import state from './../js/state';

class Placeholder extends React.Component {

  /**
   *
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = {
      def    : props.init.toString(),
      back   : props.back.toString(),
      hidden : props.value.toString()
    };
  }

  /**
   *
   * @param {Object} newProps
   */
  componentWillReceiveProps(newProps) {
    state(this, {
      hidden : newProps.value.toString(),
      back   : newProps.back.toString()
    })
  }

  /**
   *
   * @returns {XML}
   */
  render() {
    let placeholder = this.state.def
      , hidden = this.state.hidden
      , back = this.state.back;

    if (hidden && hidden.length > 0) {
      if (hidden.length >= 2 && back) {

        if (back.length > hidden.length) {

          placeholder = (
            <span className="back"><nobr><span className="hidden">{hidden}</span>{back.slice(hidden.length)}</nobr></span>)
        }

      } else {
        placeholder = '';
      }
    }

    return (
      <span className="rzhd-control__label__placeholder__inner">{placeholder}</span>
    );
  }
}

export default Placeholder;