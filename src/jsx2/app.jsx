"use strict";

import React from 'react';
import ReactDOM from 'react-dom';

import state from './../js/state';
import Form from './form';
import Screen from './screen';
import URLParser from './../js/URLParser';

var screen = true;

let headerHeight = 0, documentHeight = 0;
let container;

function postHeight(height = getDocumentHeight()) {
  // console.log('postHeight', height);
  documentHeight = height;
  if (window.parent && window.parent.postMessage) {
    window.parent.postMessage(JSON.stringify({contentHeight : height}), '*');
  }
}

function getDocumentHeight() {
  if (!container)
    container = document.getElementById('wl_railways_container');
  let info = document.getElementById('wlIndexInfo');
  if (!container)
    return document.body.scrollHeight;
  return container.clientHeight + headerHeight + (info ? info.clientHeight : 0);
}
/**
 *
 * @param {{detail:{contentHeight:number}}} event
 */
function setContentHeight(event) {
  // console.log('setContentHeight', event.detail.contentHeight, documentHeight);
  let newHeight = getDocumentHeight();

  if (documentHeight == newHeight)
    return;

  postHeight(newHeight);
}
/**
 *
 * @param {number} height
 */
function setHeaderHeight(height) {
  // console.log('setHeaderHeight', height, documentHeight);
  if (headerHeight == height)
    return;
  headerHeight = height;
  postHeight();
}

window.addEventListener('wlContentResize', setContentHeight);

class App extends React.Component {

  constructor(props) {
    super(props);

    var data = URLParser.getData();

    this.state = {
      form : !((data.from && (data.from.name || (data.from.data && data.from.data.name && data.from.data.name.ru)))
      && (data.to && (data.to.name || (data.to.data && data.to.data.name && data.to.data.name.ru)))
      && data.date),
      data : data
    };

    this.__initUrlChange();
  }

  componentDidMount() {
    this.updateHeight();

    window.addEventListener('resize', this.updateHeight.bind(this));
  }

  componentWillUpdate() {
    this.updateHeight();
  }

  /**
   *
   * @private
   */
  __initUrlChange() {
    URLParser.whenUpdate(this.updateFromUrl.bind(this));
  }

  /**
   *
   */
  updateFromUrl() {
    var data = URLParser.getData();

    // console.log('updateFromUrl', data);

    if ((data.from && (data.from.name || (data.from.data && data.from.data.name && data.from.data.name.ru)))
      && (data.to && (data.to.name || (data.to.data && data.to.data.name && data.to.data.name.ru)))
      && data.date) {
      this.screen(data);
    } else {
      this.edit(data);
    }
  }

  edit(data) {
    screen = false;
    if (this.state.form) {
      if (typeof data !== 'undefined')
        state(this, {data : data});
      return;
    }

    if (!this.refs.wrapForm || !this.refs.wrapScreen) {


      // // self.updateHeight();

      state(this, {
        form : true,
        data : (typeof data !== 'undefined' ? data : this.state.data)
      });
      return;
    }

    let self = this;

    state(this, {
      form   : true,
      fade   : true,
      height : this.refs.wrapScreen.offsetHeight,
      data   : (typeof data !== 'undefined' ? data : this.state.data)
    });

    setTimeout(function () {
      if (screen)
        return;
      state(self, {
        height : self.refs.wrapForm.offsetHeight
      });

    }, 10);

    setTimeout(function () {
      if (screen)
        return;

      // // self.updateHeight();

      state(self, {
        fade   : false,
        height : ''
      })
    }, 500);
  }

  screen(data) {
    screen = true;
    if (!this.state.form) {
      if (typeof data !== 'undefined') {

        if (!this.refs.wrapForm || !this.refs.wrapScreen) {
          state(this, {data : data});
        } else {
          let self = this;

          state(this, {
            height : this.refs.wrapScreen.offsetHeight,
            data   : data
          });

          setTimeout(function () {
            if (!screen)
              return;
            state(self, {
              height : self.refs.wrapScreen.offsetHeight
            });

          }, 10);

          setTimeout(function () {
            if (!screen)
              return;

            // self.updateHeight();

            state(self, {
              height : ''
            })
          }, 500);

        }
      }
      return;
    }

    if (!this.refs.wrapForm || !this.refs.wrapScreen) {
      state(this, {
        form : false
      });
      return;
    }

    let self = this;

    state(this, {
      form   : false,
      fade   : true,
      height : this.refs.wrapForm.offsetHeight
    });

    setTimeout(function () {
      if (!screen)
        return;
      state(self, {
        height : self.refs.wrapScreen.offsetHeight
      });

    }, 10);

    setTimeout(function () {
      if (!screen)
        return;

      // self.updateHeight();

      state(self, {
        fade   : false,
        height : ''
      })
    }, 500);
  }

  /**
   *
   * @param {MyOttStation} from
   * @param {MyOttStation} to
   * @param {Date} date
   */
  static submit(from, to, date) {
    URLParser.setUrl(from, to, date);
  }

  updateHeight() {
    setHeaderHeight(this.refs.main.clientHeight);
  }

  render() {
    return (
      <div ref="main" className="rzhd-wrapper" style={{height : (this.state.height ? this.state.height : 'auto')}}>

        <div ref="wrapForm"
             className={"block-rzhd-search__form" + (!this.state.form ? (this.state.fade ? ' rzhd-fade' : '') + ' rzhd-hide' : '')}>
          <div className="block-rzhd-search__form__title">
            <h2 className="block-rzhd-search__form__title__inner">Поиск ж/д билетов</h2>
          </div>

          <div className="block-rzhd-search__form__desc">
            <span>Совместно с </span><span className="block-rzhd-search__form__desc__image"> </span>
          </div>

          <div className="block-rzhd-search__form__inner">

            <Form data={this.state.data} app={this}/>

          </div>
        </div>

        <div ref="wrapScreen"
             className={"block-rzhd-search__screen" + (this.state.form ? (this.state.fade ? ' rzhd-fade' : '') + ' rzhd-hide' : '')}>
          <div className="block-rzhd-search__screen__title">
            <span className="block-rzhd-search__screen__title__inner">Поезда</span>
          </div>

          <Screen data={this.state.data} app={this}/>

        </div>

        <div className="block-rzhd-search__footer">
          <span>Совместно с </span><span className="block-rzhd-search__footer__image"> </span>
        </div>

      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('rzhd_form'));

export default App;